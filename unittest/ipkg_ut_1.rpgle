     /**
      * \brief iPKG : Package Information Tests
      * 
      * Assert basic package information.
      * 
      * \author Mihael Schmidt
      * \date 2014-11-30
      */
      
     H nomain

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D test_basicPackageInfo...
     D                 PR
      
      /include RPGUNIT1,TESTCASE
      /include 'ipkg_h.rpgle'
      
      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------
     P test_basicPackageInfo...
     P                 B                   export
      *
     D filePath        S           1000A
     D lead            DS                  likeds(rpm_lead_t)
      /free
       filePath = 'rpm/zlib-1.2.5-2.fc14.src.rpm';
      
       lead = bluepkg_loadLead(filePath);

       aEqual('zlib-1.2.5-2.fc14' : %trim(lead.name));
       iEqual(3 : lead.major);
       iEqual(0 : lead.minor);
       iEqual(5 : lead.sigType);
      
      /end-free
     P                 E
     
      