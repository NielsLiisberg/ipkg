# iPKG #
iPKG provides a full package management system for the IBM i platform. The main 
program is a native client (written in RPG) which can install one or more packages 
from various sources (local or the internet). 

Wouldn't it be nice to just enter one line on the console to install a program 
with all its dependencies?

The goal of this project is to provide a native client in compiled form to just 
download and run.

## What Software can I install with this client? ##
The idea is to package all open source software available for IBM i platform.

The following projects might be candidates to be packaged:

* JSON service program
* C headers for the standard C functions
* Linked list service program
* DSM (dynamic screen manager)
* HTTP client
* STOMP client
* ...

If your project is not listed here and you would like to get your software packaged 
then just drop me a mail, mihael@rpgnextgen.com .

## Installation ##
Just download the save file from the download section and restore the save file 
in the library you want. Don't forget to check the md5 hash to verify the file 
integrity. 

Simply do this from a 5250 prompt

```
qsh cmd('wget --no-check-certificate -O -  https://bitbucket.org/NielsLiisberg/ipkg/src/master/install.sh | qsh') 
```

Or you can run it step by step from ssh:


```
system -q "CRTLIB IPKG"
system -q "CRTSAVF IPKG/IPKGSAVF"
wget --no-check-certificate -N https://bitbucket.org/m1hael/ipkg/downloads/ipkgclient.savf
wget --no-check-certificate -N https://bitbucket.org/m1hael/ipkg/downloads/ipkgclient.md5
md5sum -c ipkgclient.md5
cp ipkgclient.savf /QSYS.LIB/IPKG.LIB/IPKGSAVF.FILE
system "RSTOBJ OBJ(IPKG) SAVLIB(MIHAEL) DEV(*SAVF) SAVF(IPKG/IPKGSAVF) RSTLIB(IPKG)"
```

## Getting started - Quick steps
Let's assume you will add a repo and the start install from it. Ex. you would like
to install an array list.  


```
ipkg addrepo 'rpgnextgen  http://repo.rpgnextgen.com/repository'
ipkg update
ipkg install 'arraylist' 
````




## Contribution guidelines ##

You can contribute in any way you like. Just do it! =)

Take a look at the wiki to get into the project.

## Who do I talk to? ##

Mihael Schmidt , mihael@rpgnextgen.com