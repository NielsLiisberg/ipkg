**FREE

/if not defined (SYSAPI)
/define SYSAPI

/include 'qusec_h.rpgle'

dcl-ds sys_convert_case_control_block_t qualified template;
  requestType int(10) inz(1);
  ccsid int(10) inz(0);
  convertTo int(10) inz(0);
  reserved char(10) inz(*ALLX'00');
end-ds;

dcl-pr sys_getObjectDescription extpgm('QUSROBJD');
  receiver char(1000);
  length int(10) const;
  format char(10) const;
  qualifiedObjectName char(20) const;
  type char(10) const;
end-pr;

dcl-pr sys_executeCommand extpgm('QCMDEXC');
  command char(1000) const;
  length packed(15 : 5) const;
end-pr;

dcl-pr sys_convertCase extproc('QlgConvertCase');
  controlBlock likeds(sys_convert_case_control_block_t) const;
  inString char(65535) const options(*varsize);
  outString char(65535) options(*varsize);
  length int(10) const;
  qusec likeds(qusec);
end-pr;

dcl-pr sys_checkTargetRelease extpgm('QSZCHKTG');
  targetRelease char(10) const;
  supportedReleases char(10) options(*varsize) const;
  numberSupportedReleases int(10) const;
  validatedTargetRelease char(6);
  supportedRelease char(6);
  errorCode likeds(qusec);
end-pr;

/endif
