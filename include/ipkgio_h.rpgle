**FREE

/if not defined (IPKGIO)
/define IPKGIO

//
// Constants
//
dcl-c IPKG_TYPE_BINARY 0;
dcl-c IPKG_TYPE_SOURCE 1;
dcl-c IPKG_TYPE_VIRTUAL 2;


//
// Templates
//

//  struct rpmlead {
//      unsigned char magic[4];
//      unsigned char major, minor;
//      short type;
//      short archnum;
//      char name[66];
//      short osnum;
//      short signature_type;
//      char reserved[16];
//  };

dcl-ds rpm_lead_t qualified template;
  magic char(4);
  major int(3);
  minor int(3);
  type  int(5);
  arch  int(5);
  name  char(66);
  os    int(5);
  sigType int(5);
  reserved char(16);
end-ds;

dcl-ds rpm_header_structure_header_t qualified template;
  magic char(3);
  version int(3);
  reserved char(4);
  data1 char(4);
  numberEntries int(10) overlay(data1);
  data2 char(4);
  dataLength int(10) overlay(data2);
end-ds;

dcl-ds rpm_index_entry_t qualified template;
  tag    int(10);
  type   int(10);
  offset int(10);
  count  int(10);
end-ds;

dcl-s ipkg_path_t char(1024) template;
dcl-s ipkg_header_name_t varchar(100) template;
dcl-c IPKG_HEADER_VALUE_LENGTH 10240;
dcl-s ipkg_header_value_t varchar(IPKG_HEADER_VALUE_LENGTH) template;

dcl-ds ipkg_rpm_header_entry_t qualified template;
  id int(10);
  name varchar(100);
  type char(2);
  length int(10);
  value pointer;
end-ds;


//
// Prototypes
//

dcl-pr ipkg_io_isRpmFile ind extproc(*dclcase);
  lead likeds(rpm_lead_t) const;
end-pr;

dcl-pr ipkg_io_isFileFormatSupported ind extproc(*dclcase);
  lead likeds(rpm_lead_t) const;
end-pr;

dcl-pr ipkg_io_readSignatureSection pointer extproc(*dclcase);
  fileHandle int(10) const;
end-pr;

dcl-pr ipkg_io_readHeaderSection pointer extproc(*dclcase);
  fileHandle int(10) const;
end-pr;

dcl-pr ipkg_io_open int(10) extproc(*dclcase);
  path like(ipkg_path_t) const;
end-pr;

dcl-pr ipkg_io_readLead likeds(rpm_lead_t) extproc(*dclcase);
  fileHandle int(10) const;
end-pr;

dcl-pr ipkg_io_disposeHeaders extproc(*dclcase);
  headers pointer;
end-pr;

dcl-pr ipkg_io_setToArchivePosition extproc(*dclcase);
  fileHandle int(10) const;
end-pr;
  
/endif