**FREE

/if not defined (IPKGREQPRD)
/define IPKGREQPRD

dcl-pr ipkg_req_doesMeetProductRequirement ind extproc(*dclcase);
  requiredProduct char(7) const;
  version char(5) const;
  type int(10) const;
end-pr;

/endif
