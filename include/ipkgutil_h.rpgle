**FREE

/if not defined (IPKGUTIL)
/define IPKGUTIL

/include 'ipkgio_h.rpgle'

dcl-c IPKG_UTIL_TYPE_SAVEFILE 1;
dcl-c IPKG_UTIL_TYPE_ZIP 2;

dcl-ds ipkg_util_journalInfo_t qualified template;
  enabled ind;
  name char(10);
  library char(10);
end-ds;

dcl-ds ipkg_util_version_t qualified template;
  major int(10) inz(0);
  minor int(10) inz(0);
  revision int(10) inz(0);
  build int(10) inz(0);
end-ds;

dcl-pr ipkg_util_translateFromUtf8 char(IPKG_HEADER_VALUE_LENGTH) extproc(*dclcase);
   string pointer const;
   pLength uns(10) const;
end-pr;

dcl-pr ipkg_util_fileExist ind extproc(*dclcase);
  filePath like(ipkg_path_t) const;
end-pr;

dcl-pr ipkg_util_fileReadable ind extproc(*dclcase);
  filePath like(ipkg_path_t) const;
end-pr;

dcl-pr ipkg_util_init int(10) extproc(*dclcase);
  ipkgLibrary char(10) const;
end-pr;

dcl-pr ipkg_util_cleanup extproc(*dclcase) end-pr;

dcl-pr ipkg_util_alloc pointer extproc(*dclcase);
  size uns(10) const;
end-pr;

dcl-pr ipkg_util_getHeaderEntryById likeds(ipkg_rpm_header_entry_t) extproc(*dclcase);
  headers pointer const;
  id int(10) const;
end-pr;

dcl-pr ipkg_util_objectExist ind extproc(*dclcase);
  library char(10) const;
  name char(10) const;
  type char(10) const;
end-pr;

dcl-pr ipkg_util_executeScript extproc(*dclcase);
  script varchar(10000) const;
end-pr;

dcl-pr ipkg_util_determineObjectName char(10) extproc(*dclcase);
  ifsPath like(ipkg_path_t) const;
end-pr;
  
dcl-pr ipkg_util_determineObjectType char(10) extproc(*dclcase);
  ifsPath like(ipkg_path_t) const;
end-pr;

dcl-pr ipkg_util_determineInstallLibrary char(10) extproc(*dclcase);
  ipkgLibrary char(10) const;
  objectLibrary char(10) const;
  dataLibrary char(10) const;
  headers pointer const;
end-pr;

dcl-pr ipkg_util_checkIpkgDataArea ind extproc(*dclcase);
  ipkgLibrary char(10) const;
end-pr;

dcl-pr ipkg_util_getJournalInfo likeds(ipkg_util_journalInfo_t) extproc(*dclcase);
  library char(10) const;
  name char(10) const;
end-pr;

dcl-pr ipkg_util_toUpperCase varchar(65530) extproc(*dclcase);
  string varchar(65530) const;
end-pr;

dcl-pr ipkg_util_comparePackageVersion int(10) extproc(*dclcase);
  version1 char(20) const;
  version2 char(20) const;
  ignoreBuildNumber ind const options(*nopass);
end-pr;

dcl-pr ipkg_util_relocate varchar(1000) extproc(*dclcase);
  path like(ipkg_path_t) const;
  prefix varchar(1000) const;
  location varchar(1000) const;
end-pr;

dcl-pr ipkg_util_stringEndsWith ind extproc(*dclcase);
  s varchar(10000) const;
  value varchar(1000) const;
end-pr;

dcl-pr ipkg_util_stringStartsWith ind extproc(*dclcase);
  s varchar(10000) const;
  value varchar(1000) const;
end-pr;

dcl-pr ipkg_util_determineLibraryName varchar(10) extproc(*dclcase);
  path varchar(100) const;
end-pr;

dcl-pr ipkg_util_getArchiveType int(10) extproc(*dclcase);
  headers pointer const;
end-pr;

dcl-pr ipkg_util_getPackageFiles pointer extproc(*dclcase);
  headers pointer const;
end-pr;

dcl-pr ipkg_util_getBaseInstallFolder varchar(1000) extproc(*dclcase);
  create ind const options(*nopass);
end-pr;

dcl-pr ipkg_util_assertDependency ind extproc(*dclcase);
  version char(20) const;
  expectedVersion char(20) const;
  type int(10) const;
end-pr;

dcl-pr ipkg_util_parsePackageVersion likeds(ipkg_util_version_t) extproc(*dclcase);
  version char(20) const;
end-pr;

dcl-pr ipkg_util_versionToString char(6) extproc(*dclcase);
  version likeds(ipkg_util_version_t) const;
end-pr;

/endif