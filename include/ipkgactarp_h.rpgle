**FREE

/if not defined (IPKGACTARP)
/define IPKGACTARP

dcl-pr ipkg_action_addRepo extproc(*dclcase);
  ipkgLibrary char(10) const;
  name varchar(50) const;
  url varchar(1000) const;
end-pr;

/endif
