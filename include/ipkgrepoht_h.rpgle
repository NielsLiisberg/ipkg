**FREE

/if not defined (IPKGREPOHTTP)
/define IPKGREPOHTTP

dcl-pr ipkg_repo_http_create pointer extproc(*dclcase) end-pr;

dcl-pr ipkg_repo_http_updateRepository extproc(*dclcase);
  ipkgLibrary char(10) const;
  repoId int(10) const;
  repoUrl varchar(1000) const;
end-pr;

dcl-pr ipkg_repo_http_retrievePackage varchar(1000) extproc(*dclcase);
  ipkgLibrary char(10) const;
  repoUrl varchar(1000) const;
  packageUrl varchar(1000) const;
end-pr;

/endif
