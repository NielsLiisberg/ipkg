**FREE

/if not defined (IPKGACTINS)
/define IPKGACTINS

dcl-pr ipkg_action_installPackage extproc(*dclcase);
  ipkgLibrary char(10) const;
  location varchar(1000) const;
  dataLibrary char(10) const;
  filePath like(ipkg_path_t) const;
  owner char(10) const;
end-pr;
  
/endif