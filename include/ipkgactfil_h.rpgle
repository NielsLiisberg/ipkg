**FREE

/if not defined (IPKGACTFIL)
/define IPKGACTFIL

/include 'ipkgio_h.rpgle'

dcl-pr ipkg_action_showFiles extproc(*dclcase);
  filePath like(ipkg_path_t) const;
end-pr;

/endif
